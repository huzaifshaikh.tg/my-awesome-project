/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import {
  View,
} from 'react-native';

import FullScreen from './src/components/FullScreen';

const App = () => {

  const backgroundStyle = {
    backgroundColor: '#f8f9fa',
    paddingLeft: 20,
    paddingRight: 20,
  };

  return (
    <>
      <View
        // contentInsetAdjustmentBehavior="automatic"
        style={backgroundStyle}
      >
        <FullScreen />
      </View>
    </>
  );
}

export default App;
