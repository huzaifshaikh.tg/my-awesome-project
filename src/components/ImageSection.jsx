import React from 'react'
import { Image, View } from 'react-native'

import { nullImg } from '../assets/images/NullImg'

export default function ImageSection({ src, style, height, width, borderWidth, borderColor }) {
    return (
        <View style={style}>
            <Image style={{ height: height || 90, width: width || 90, borderWidth: borderWidth || 0, borderRadius: 50, borderColor: borderColor || 0 }} source={{ uri: src || nullImg }} />
        </View>
    )
}