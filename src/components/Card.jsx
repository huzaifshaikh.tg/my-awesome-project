import React from 'react'
import { ScrollView, StyleSheet, Text, View } from 'react-native'

import ImageSection from './ImageSection'
import Ranking from './Ranking'

export default function Card({ data }) {
    let information = data.slice(0)
    return (
        <ScrollView contentInsetAdjustmentBehavior="automatic">
            {information && information.map(item => (
                <View key={item.id} style={style.mainAlignment}>
                    <View style={style.textAlignment}>
                        <Text style={style.textStyling}>{item.id}</Text>
                        <ImageSection src={item.image} height={50} width={50} />
                        <View style={style.detailStyling}>
                            <Text style={style.textStyling}>{item.name}</Text>
                            <Text style={style.countryStyling}>{item.country}</Text>
                        </View>
                    </View>
                    <Ranking active={item.activeIcon} points={item.vote} fontSize={18} />
                </View>))}
        </ScrollView>
    )
}

const style = StyleSheet.create({
    mainAlignment: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderRadius: 5,
        borderColor: '#000',
        backgroundColor: '#fff',
        marginTop: 10,
        padding: 10,
    },
    textAlignment: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        gap: 10,
    },
    textStyling: {
        fontSize: 18,
        color: "#000",
        marginLeft: 10,
        marginRight: 5,
    },
    countryStyling: {
        fontSize: 12,
        color: "#000",
        marginLeft: 10,
    },
    detailStyling: {
        flexDirection: 'column',
    }
})