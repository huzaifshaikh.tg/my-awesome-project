import React from 'react'
import {
    ScrollView,
    StyleSheet,
    Text,
    View,
} from 'react-native';

import Card from './Card';
import TopRanks from './TopRanks';
import { Data } from '../helpers/Data'

export default function FullScreen() {
    return (
        <>
            <View>
                <Text style={style.title}>Leaderboard</Text>
            </View>
            <ScrollView contentInsetAdjustmentBehavior="automatic">
                <View style={style.main}>
                    <TopRanks data={Data} />
                    <Card data={Data} />
                </View>
            </ScrollView>
        </>
    )
}

const style = StyleSheet.create({
    title: {
        textAlign: 'center',
        fontSize: 30,
        marginTop: 10,
        marginBottom: 10,
        color: '#000',
    },
    main: {
        marginTop: 50,
        marginBottom: 100,
    }
})