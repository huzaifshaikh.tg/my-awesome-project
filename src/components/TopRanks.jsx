import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

import ImageSection from './ImageSection'
import Ranking from './Ranking'

export default function TopRanks({ data }) {
    const heightMapper = {
        1: '80%',
        2: '70%',
        3: '60%',
        default: '50%',
    };

    const colorMapper = {
        1: '#7209b7',
        2: '#00b4d8',
        3: '#ffc300',
        default: '#7678ed',
    };

    const borderMapper = {
        1: '#7209b7',
        2: '#00b4d8',
        3: '#ffc300',
        default: '#7678ed',
    };
    let information = data.splice(0, 3);
    return (
        <View style={style.Main}>
            {information && information.map(item => {
                const height = heightMapper[item.id] || heightMapper.default;
                const backgroundColor = colorMapper[item.id] || colorMapper.default;
                const borderColor = borderMapper[item.id] || borderMapper.default;
                return <View key={item.id}>
                    <View style={[style.RankBar, { height }, { backgroundColor }]}>
                        <ImageSection style={style.imageStye} src={item.image} height={90} width={90} borderWidth={6} borderColor={borderColor} />
                        <View style={style.RanksStyle}>
                            <Text style={style.Rank}>{item.id}</Text>
                            <Text style={style.name}>{item.name || "-"}</Text>
                        </View>
                        <Ranking active={item.activeIcon} points={item.vote} color={`#fff`} />
                    </View>
                </View>
            })}
        </View>
    )
}

const style = StyleSheet.create({
    Main: {
        flexDirection: "row",
        alignItems: 'flex-end',
        justifyContent: "space-between",
        height: 300,
        backgroundColor: 'transparent',
    },
    RanksStyle: {
        flexDirection: 'column',
        justifyContent: 'top',
        alignItems: 'center',
    },
    RankBar: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: '#fff',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 0,
        paddingLeft: 15,
        paddingRight: 15,
    },
    Rank: {
        fontSize: 50,
        alignItems: 'center',
        justifyContent: "flex-start",
        color: `#fff`,
    },
    name: {
        fontSize: 12,
        width: 60,
        textAlign: 'center',
        justifyContent: "flex-start",
        color: `#fff`,
    },
    imageStye: {
        position: 'absolute',
        top: -95,
    }
})
