import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'

export default function Ranking({ active, points, color, fontSize }) {
    const icon = active ? require('../assets/images/high.png') : require('../assets/images/low.png')
    return (
        <View style={style.alignment}>
            <Text style={[style.textStyling, { color: color || "#000" }, { fontSize: fontSize || 12 }]}>{`${points} pts` || "NA"}</Text>
            <Image style={style.imageStyle} source={icon} />
        </View>
    )
}

const style = StyleSheet.create({
    alignment: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        gap: 5,
        margin: 10,
    },
    imageStyle: {
        height: 15,
        width: 15,
        marginBottom: 5
    },
    textStyling: {
        fontSize: 12,
        color: "#000",
    }
})